package com.redhat.movie.Domain.Interfaces;

import com.redhat.movie.Domain.Resources.MovieResources;
import java.util.List;

public interface ApiMovieRepository {
    List<MovieResources> getMovies() throws InterruptedException;
}