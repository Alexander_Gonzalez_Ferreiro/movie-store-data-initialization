package com.redhat.movie.Domain.Searcher;

import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Infraestructure.Repository.ApiMovieDbRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class MovieSearcher {

    @Inject
    ApiMovieDbRepository repository;

    public List<MovieResources> execute() throws InterruptedException {
        return repository.getMovies();
    }

}
