package com.redhat.movie.Domain.Creator;

import com.redhat.movie.Domain.Entity.Movie;
import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Domain.Resources.ResultMoviesResources;
import org.elasticsearch.client.Request;

import io.vertx.core.json.JsonObject;
import org.elasticsearch.client.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@ApplicationScoped
public class MovieCreator {

    @Inject
    RestClient restClient;

    @Transactional
    public void execute(ResultMoviesResources movies) throws IOException {
        for (MovieResources movie:movies.results) {
            Movie movieEntity = new Movie();

            List<Integer> genres = new ArrayList<>(movie.genre_ids);

            movieEntity.title = movie.title;
            movieEntity.main_genre = genres.get(0);
            movieEntity.posterUrl = "https://image.tmdb.org/t/p/w500/"+movie.poster_path;
            movieEntity.price = this.setPrice();

            Request request = new Request(
                "PUT",
                "/movies/_doc" + movie.poster_path);
            request.setJsonEntity(JsonObject.mapFrom(movieEntity).toString());
            restClient.performRequest(request);
        }
    }

    private float setPrice() {
        int min = 0;
        int max = 9;

        Random random = new Random();

        int value = random.nextInt(max + min) + min;
        return (float) ((float) value + 0.99);
    }
}
