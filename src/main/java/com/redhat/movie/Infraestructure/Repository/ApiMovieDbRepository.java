package com.redhat.movie.Infraestructure.Repository;

import com.redhat.movie.Domain.Interfaces.ApiMovieRepository;
import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Domain.Resources.ResultMoviesResources;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

@ApplicationScoped
public class ApiMovieDbRepository implements ApiMovieRepository {

    @ConfigProperty(name = "api.moviedb.key")
    String api_key;

    @ConfigProperty(name = "movie.host", defaultValue = "https://api.themoviedb.org/3/movie/popular")
    String movieHost;

    private final Client client = ClientBuilder.newClient();

    public List<MovieResources> getMovies() throws InterruptedException {
        List<MovieResources> result = new ArrayList<>();
        for (int i = 1; i < 15; i++) {
            ResultMoviesResources response = client.target(movieHost)
                .queryParam("api_key", api_key)
                .queryParam("page",i)
                .request(MediaType.APPLICATION_JSON)
                .get(ResultMoviesResources.class);
            result.addAll(response.results);
            sleep(2000);
        }

        return result;
    }
}
