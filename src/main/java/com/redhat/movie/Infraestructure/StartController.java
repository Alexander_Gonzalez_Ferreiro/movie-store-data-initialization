package com.redhat.movie.Infraestructure;

import com.redhat.movie.Application.GetAndSaveInitialMoviesUseCase;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@ApplicationScoped
public class StartController {

    private static final Logger LOGGER = LoggerFactory.getLogger("ListenerStartController");

    @Inject
    GetAndSaveInitialMoviesUseCase useCase;

    void onStart(@Observes StartupEvent ev) throws InterruptedException, IOException {
        LOGGER.info("The application is starting.");
        useCase.run();
        LOGGER.info("The application is finish starting methods.");
        Quarkus.asyncExit();
    }

    void onStop(@Observes ShutdownEvent ev) {
        LOGGER.info("The application is stopping.");
    }
}
