package com.redhat.movie.Application;

import com.redhat.movie.Domain.Creator.MovieCreator;
import com.redhat.movie.Domain.Resources.MovieResources;
import com.redhat.movie.Domain.Resources.ResultMoviesResources;
import com.redhat.movie.Domain.Searcher.MovieSearcher;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@ApplicationScoped
public class GetAndSaveInitialMoviesUseCase {

    @Inject
    MovieSearcher searcher;

    @Inject
    MovieCreator creator;

    public void run() throws InterruptedException, IOException {
        List<MovieResources> movies = searcher.execute();

        ResultMoviesResources resultMovies = new ResultMoviesResources();
        resultMovies.results = movies;
        resultMovies.total_results = movies.size();

        creator.execute(resultMovies);
    }

}
